<?php

namespace spec;

use OddEven;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OddEvenSpec extends ObjectBehavior
{
    function it_is_0()
    {
        $this->shouldThrow(new \InvalidArgumentException('Number must be large than 1' ))->duringgenerate('0');
    }
    function it_is_number_1()
    {
        $this->generate(1)->shouldBe(['odd']);
    }

    function it_is_even_2()
    {
        $this->generate(2)->shouldBe(['odd','even']);
    }
    function it_is_5()
    {
        $this->generate(5)->shouldBe(['odd','even',3,'even',5]);
    }
    function it_is_101()
    {
        $this->shouldThrow( new \InvalidArgumentException('Number must be smaller than 100' ))->duringgenerate('101');;
    }
    function it_is_102()
    {
        $this->shouldThrow( new \InvalidArgumentException('Number must be smaller than 100' ))->duringgenerate('102');;
    }
    function it_is_g_2()
    {
        $this->shouldThrow( new \InvalidArgumentException('Number must be smaller than 100' ))->duringgenerate('102');;
    }
}
