<?php

class OddEven
{

    const MIN = 1;
    const MAX = 100;
    const EVEN = 'even';
    const ODD = 'odd';

    public function convert($number){
       
        if($this->isEven($number)){
            return  self::EVEN; 
    
        }
        else if($this->isPrime($number)){
            return $number;
        }
        else  return  self::ODD;
    }
    /**
     * @return array
     * @param interger
     */
    public function generate($number){
        if(!is_numeric($number)){
            throw new InvalidArgumentException('invalid number: '.$number);
        }
        if($number < self::MIN){
            throw new InvalidArgumentException('Number must be large than '.self::MIN );
        }
        if($number > self:: MAX){
            throw new InvalidArgumentException('Number must be smaller than '.self::MAX);
        }
        $arr_res = [];
        for($candiate =self::MIN; $candiate <= $number; $candiate ++){
            $arr_res[] = $this->convert( $candiate );
        }
        return $arr_res;

    }
    public function isEven($number){
        return $number % 2 == 0;
    }
    public function isPrime($number){
        if($number < 2){
            return false;
        }
        for($i = 2; $i < $number ; $i ++){
            if($number % $i == 0){
                return false;
            }
        }
        return true;
    }

}
